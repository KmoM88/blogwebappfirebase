import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { BehaviorSubject } from 'rxjs';

import { Post } from './../interfaces/Post';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private credentials: firebase.auth.AuthCredential;
  private user: firebase.User;
  public displayName = '';
  public uid = '';
  public email = '';
  post: Post = {
    usrId: '',
    name: '',
    title: '',
    body: '',
    date: 0,
    uid: ''
  };
  isInFavs: boolean;

  constructor(
    private auth: AngularFireAuth,
    private afStore: AngularFirestore,
    ) {
      this.authStatusListener();
      auth.authState.subscribe(user => {
        this.user = user;
      });
    }

  currentUser = null;
  private authStatusSub$ = new BehaviorSubject(this.currentUser);
  currentAuthStatus$ = this.authStatusSub$.asObservable();

  authStatusListener(){
    this.auth.onAuthStateChanged((credential) => {
      if (credential){
        // console.log(credential);
        this.uid = credential.uid;
        this.displayName = credential.displayName;
        this.email = credential.email;
        this.authStatusSub$.next(credential);
        console.log('User is logged in');
      }
      else{
        // console.log(credential);
        this.authStatusSub$.next(null);
        console.log('User is logged out');
      }
    });
  }

  signOut(){
    return this.auth.signOut();
  }

  createPost(post) {
    try {
      const id = this.afStore.createId();
      this.afStore.doc(`posts/${id}`).set({
        name: this.displayName,
        body: post.body,
        usrId: post.usrId,
        title: post.title,
        date: Date.now(),
        uid: id
      });
      this.afStore.doc(`users/${post.usrId}/posts/${id}`).set({
        post: {
          name: this.displayName,
          body: post.body,
          usrUid: post.usrId,
          title: post.title,
          date: Date.now(),
          uid: id
        }
      });
    } catch (err) {
      console.dir(err);
    }
  }

  deletePost(postId, usrId): any {
    this.afStore.doc(`posts/${postId}`).delete().then((res) => {
      console.log('Document successfully deleted from posts collection!');
    }).catch((error) => {
      console.error('Error removing document: ', error);
    });
    this.afStore.doc(`users/${usrId}/posts/${postId}`).delete().then((res) => {
      console.log('Document successfully deleted users collection!');
    }).catch((error) => {
      console.error('Error removing document: ', error);
    });
  }

  favPost(post, uid) {
    this.afStore.doc(`users/${uid}/fav/${post.uid}`)
      .snapshotChanges()
      .subscribe(x => {
        if (x.payload.exists) {
          console.log('Exists', x.payload.exists);
          this.isInFavs = true;
        }
        else {
          console.log('!Exists', x.payload.exists);
          this.isInFavs = false;
        }
      }
    );
    if (this.isInFavs) {
      this.afStore.doc(`users/${uid}/fav/${post.uid}`).delete().then((res) => {
        console.log('Document successfully deleted from fav posts collection!');
        return 'Post quitado a favoritos';
    }).catch((error) => {
        console.error('Error removing document: ', error);
    });
    }
    else {
      this.afStore.doc(`users/${uid}/fav/${post.uid}`).set(
      post
      ).then((res) => {
        console.log('Document successfully added to fav posts collection!', res);
        return 'Post agregado a favoritos';
      }).catch((error) => {
        console.error('Error removing document: ', error);
      });
    }
  }

  getId() {

  }

  getPosts() {

  }
}
