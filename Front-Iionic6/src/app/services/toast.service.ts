import { Injectable } from '@angular/core';

import { ToastController, LoadingController, AlertController } from '@ionic/angular';

import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  loading: any;

  constructor(
    private toastController: ToastController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private httpService: HttpService,
  ) { }

  async showToast(header: string, message: string) {
    const toast = await this.toastController.create({
      header,
      message,
      duration: 2500,
      position: 'bottom'
    });
    await toast.present();
  }

  async presentLoading(msg: string) {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: msg,
    });
    return await this.loading.present();
  }

  async presentAlert(postId: string, usrId: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Borrado de contenido',
      message: 'Está seguro que desea borrar este post?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.showToast('Acción cancelada', '');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.httpService.deletePost(postId, usrId);
            this.showToast('Éxito', 'Post borrado');
          }
        }
      ]
    });

    await alert.present();
  }
}

