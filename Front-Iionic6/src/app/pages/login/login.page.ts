import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';

import { UserLogin } from '../../interfaces/User';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('formulario') formulario: NgForm;

  user: UserLogin = {
    email: '',
    password: '',
  };
  serverAnswer = '';

  constructor(
    private toastService: ToastService,
    private router: Router,
    private auth: AngularFireAuth,
    private afStore: AngularFirestore,
    ) { }

  ngOnInit() {
  }

  async onLoginSubmit(event) {
    if (!this.formulario.valid) {
      if (this.formulario.controls.password.invalid) {
        this.toastService.showToast('Error', 'Invalid Password');
      }
      if (this.formulario.controls.email.invalid) {
        this.toastService.showToast('Error', 'Invalid Mail');
      }
    }
    else {
      try {
        this.toastService.presentLoading('Espere');
        const res = await this.auth.signInWithEmailAndPassword(this.user.email, this.user.password);
        if (res.user){
          this.toastService.showToast('Exito', 'Bienvenido de nuevo!');
          this.toastService.loading.dismiss();
          this.router.navigate(['posts']);
        }
      } catch (err) {
        console.dir(err);
        if (err.code === 'auth/user-not-found'){
          this.toastService.showToast('Error', 'User not found');
        }
      }
    }
  }

  loginGoogle() {
    this.toastService.presentLoading('Espere');
    this.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        this.saveUsr(res);
      })
      .catch(err => {
        this.toastService.loading.dismiss();
        this.toastService.showToast('Error', err.message);
      });
  }

  loginFacebook() {
    console.log(firebase.auth().currentUser);
    // this.auth
    //   .signInWithPopup(new firebase.auth.FacebookAuthProvider())
    //   .then(res => {
    //     this.httpService.setCredential(res.credential);
    //     this.showToast('Exito', 'Bienvenido!');
    //     this.router.navigate(['tabs']);
    //   })
    //   .catch(err => {
    //     this.showToast('Error', err.message);
    //   });
  }

  loginTwitter() {
    console.log(firebase.auth().currentUser);
    // this.auth
    //   .signInWithPopup(new firebase.auth.TwitterAuthProvider())
    //   .then(res => {
    //     this.httpService.setCredential(res.credential);
    //     this.showToast('Exito', 'Bienvenido!');
    //     this.router.navigate(['tabs']);
    //   })
    //   .catch(err => {
    //     this.showToast('Error', err.message);
    //   });
  }

  signup() {
    this.router.navigate(['signup']);
  }

  saveUsr(res) {
    this.afStore.doc(`users/${res.user.uid}`).set({
      name: res.user.displayName,
      email: res.user.email,
      avatarUrl: '',
      lastLogin: Date.now()
    });
    this.toastService.loading.dismiss();
    this.toastService.showToast('Exito', 'Bienvenido!');
    this.router.navigate(['posts']);
  }
}
