import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    private httpService: HttpService,
    private router: Router,
  ) { }

  ngOnInit() {
  }


  logOut() {
    this.httpService.signOut();
    this.router.navigate(['home']);
  }
}
