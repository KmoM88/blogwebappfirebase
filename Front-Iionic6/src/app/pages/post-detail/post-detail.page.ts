import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { PostServer } from './../../interfaces/Post';
import { HttpService } from './../../services/http.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.page.html',
  styleUrls: ['./post-detail.page.scss'],
})
export class PostDetailPage implements OnInit {

  private postCollection: AngularFirestoreCollection<PostServer>;
  postSel: Observable<PostServer[]>;
  loadedPost: PostServer;
  usrUid = '';
  currentAuthStatus$: Observable<any>;
  titulo = 'Post Detalle';

  constructor(
    private toastService: ToastService,
    private httpService: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private afStore: AngularFirestore
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.currentAuthStatus$ = this.httpService.currentAuthStatus$;
      this.usrUid = this.httpService.uid;
      this.activatedRoute.paramMap.subscribe(paramMap => {
        if (!paramMap.has('postId')){
          return;
        }
        const postId = paramMap.get('postId');
        this.postCollection = this.afStore.collection<PostServer>('posts');
        this.postSel = this.postCollection.valueChanges().pipe(
          map(postArray => {
            return postArray.filter(loadedPost => loadedPost.uid === postId)
          })
        );
        this.postSel.subscribe(x => {
          this.loadedPost = x[0];
          this.titulo = this.loadedPost.title;
          // console.log('TEST', this.loadedPost.usrId);
          // console.log(this.usrUid);
        });
      });
    });
  }

  editPost() {

  }

  deletePost() {
    if (this.loadedPost.usrId === this.usrUid){
      this.toastService.presentAlert(this.loadedPost.uid, this.loadedPost.usrId);
      this.router.navigate(['posts']);
    }
    else{
      this.toastService.showToast('Error', 'Solo puedes borrar tus posts');
    }
  }

  favoritePost() {
    console.log(this.loadedPost);
    console.log(this.usrUid);
    this.httpService.favPost(this.loadedPost, this.usrUid);
  }

  sharePost() {

  }
}
