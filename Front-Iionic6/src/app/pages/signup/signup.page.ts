import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';

import { UserSignup } from '../../interfaces/User';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('formulario') formulario: NgForm;

  user: UserSignup = {
    name: '',
    email: '',
    password: ''
  };
  serverAnswer = '';

  constructor(
    private toastService: ToastService,
    private router: Router,
    private auth: AngularFireAuth,
    private afStore: AngularFirestore,
    ) { }

  ngOnInit() {
  }

  async onUserSubmit(event) {
    if (!this.formulario.valid) {
      if (this.formulario.controls.password.invalid) {
        this.toastService.showToast('Error', 'Invalid Password');
      }
      if (this.formulario.controls.email.invalid) {
        this.toastService.showToast('Error', 'Invalid Mail');
      }
      if (this.formulario.controls.name.invalid) {
        this.toastService.showToast('Error', 'Invalid Name');
      }
    }
    else {
      this.toastService.presentLoading('Espere');
      try {
        await this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password)
        .then(async (user) => {
          await user.user.updateProfile({displayName: this.user.name});
          const res = firebase.auth().currentUser;
          console.log('TEST', res);
          this.saveUsr(res);
        });
      }
      catch (err) {
        this.toastService.loading.dismiss();
        console.dir(err);
        this.toastService.showToast('Error', err.message);
      }
    }
  }

  loginGoogle() {
    this.toastService.presentLoading('Espere');
    this.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        this.saveUsr(res);
      })
      .catch(err => {
        this.toastService.loading.dismiss();
        this.toastService.showToast('Error', err.message);
      });
  }

  loginFacebook() {
    console.log(firebase.auth().currentUser);
    // this.auth
    //   .signInWithPopup(new firebase.auth.FacebookAuthProvider())
    //   .then(res => {
    //     this.httpService.setCredential(res.credential);
    //     this.showToast('Exito', 'Bienvenido!');
    //     this.router.navigate(['tabs']);
    //   })
    //   .catch(err => {
    //     this.showToast('Error', err.message);
    //   });
  }

  loginTwitter() {
    console.log(firebase.auth().currentUser);
    // this.auth
    //   .signInWithPopup(new firebase.auth.TwitterAuthProvider())
    //   .then(res => {
    //     this.httpService.setCredential(res.credential);
    //     this.showToast('Exito', 'Bienvenido!');
    //     this.router.navigate(['tabs']);
    //   })
    //   .catch(err => {
    //     this.showToast('Error', err.message);
    //   });
  }

  login() {
    this.router.navigate(['login']);
  }

  async saveUsr(res) {
    console.log('USER NAME NOT NULL');
    this.afStore.doc(`users/${res.uid}`).set({
      name: res.displayName,
      email: res.email,
      avatarUrl: '',
      lastLogin: Date.now(),
      posts: []
    });

    this.toastService.loading.dismiss();
    this.toastService.showToast('Exito', 'Bienvenido!');
    this.router.navigate(['posts']);
  }
}
