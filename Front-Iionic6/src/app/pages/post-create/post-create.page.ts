import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Post } from './../../interfaces/Post';
import { HttpService } from './../../services/http.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.page.html',
  styleUrls: ['./post-create.page.scss'],
})
export class PostCreatePage implements OnInit {

  constructor(
    private toastService: ToastService,
    private httpService: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    ) { }

  post: Post = {
    usrId: '',
    name: '',
    title: '',
    body: '',
    date: Date.now(),
    uid: ''
  };

  @ViewChild('formulario') formulario: NgForm;

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.post.name = this.httpService.displayName;
      this.post.usrId = this.httpService.uid;
    });
  }

  async onPostSubmit() {
    await this.toastService.presentLoading('Espere');
    this.httpService.createPost(this.post);
    this.toastService.loading.dismiss();
    this.router.navigate(['posts']);
    //   res => {
    //     this.serverAnswer = JSON.stringify(res);
    //     // console.log(this.serverAnswer);
    //     this.router.navigate(['/posts']);
    //   },
    //   error => {
    //     this.serverAnswer = JSON.stringify(error);
    //     console.log(this.serverAnswer);
    //   }
    // );
  }
}
