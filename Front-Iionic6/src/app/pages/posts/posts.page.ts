import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs';

import { IonList, } from '@ionic/angular';

import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { HttpService } from './../../services/http.service';
import { PostServer } from './../../interfaces/Post';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {

  @ViewChild('list') list: IonList;

  private postCollection: AngularFirestoreCollection<PostServer>;
  posts: Observable<PostServer[]>;
  currentAuthStatus$: Observable<any>;
  uid = '';

  constructor(
    private toastService: ToastService,
    private httpService: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private afStore: AngularFirestore
    ) {
      this.postCollection = afStore.collection<PostServer>('posts');
      this.posts = this.postCollection.valueChanges();
    }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.currentAuthStatus$ = this.httpService.currentAuthStatus$;
      this.uid = this.httpService.uid;
    });
  }

  favoritePost(post) {
    this.httpService.favPost(post, this.uid);
    this.list.closeSlidingItems();
  }

  sharePost(post) {
    console.log('Compartir');
    this.list.closeSlidingItems();
  }

  deletePost(post){
    if (post.usrId === this.uid){
      this.toastService.presentAlert(post.uid, post.usrId);
    }
    else{
      this.toastService.showToast('Error', 'Solo puedes borrar tus posts');
    }
    this.list.closeSlidingItems();
  }
}
