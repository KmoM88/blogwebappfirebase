import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';

import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  currentAuthStatus$: Observable<any>;
  displayName = '';

  constructor(
    private httpService: HttpService,
    private activatedRoute: ActivatedRoute,
    // private auth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.currentAuthStatus$ = this.httpService.currentAuthStatus$;
      this.displayName = this.httpService.displayName;
    });
  }
}
