import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { SlidesHomeComponent } from './slides-home/slides-home.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    SlidesHomeComponent
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    SlidesHomeComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class ComponentsModule { }
