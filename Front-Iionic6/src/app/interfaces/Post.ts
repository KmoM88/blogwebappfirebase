export interface Post {
  usrId: string;
  name: string;
  title: string;
  body: string;
  date: number;
  uid: string;
}

export interface PostServer {
  body: string;
  date: string;
  name: string;
  title: string;
  uid: string;
  usrId: string;
}
