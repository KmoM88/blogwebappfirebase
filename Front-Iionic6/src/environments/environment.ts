// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBPGsAIMWUIs_qO7EM88j_f2V8DrvZgE5o',
    authDomain: 'flores-museo.firebaseapp.com',
    projectId: 'flores-museo',
    storageBucket: 'flores-museo.appspot.com',
    messagingSenderId: '763904998416',
    appId: '1:763904998416:web:3a27bc7377737aad4b1e95',
    measurementId: 'G-5B38XFWPHH'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
